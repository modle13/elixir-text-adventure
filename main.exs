#! /usr/bin/elixir


defmodule Game do
    def directions do
        %{"west" => "you went west", "east" => "you went east"}
    end

    def print(to_print) do
        IO.puts to_print
    end

    def run("start") do
        print "starting"
        run("getinput")
    end

    def run("exit") do 
        print "exiting"
    end

    def run(command) do
        old_command = command
        new_command = IO.gets "What is your command "
        new_command = String.trim(new_command)
        print "old command was #{old_command}"
        print "new command is #{new_command}"
        action = Map.get(directions(), new_command)
        cond do
            action ->
                print(action)
            !action ->
                print("i tried")
        end
        run(String.trim(new_command))
    end
end

Game.run("start")
